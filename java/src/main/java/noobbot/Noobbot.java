package noobbot;

public abstract class Noobbot {
    int msgCount=-1;

    
	abstract SendMsg nextMove( );	

	double getDistanceToCurve(CarPosition myCar)
	{
		double distance = 0;
		
		Piece currentPiece = RaceState.I().getPiece(myCar.piecePosition.pieceIndex);
		if(Math.abs(currentPiece.angle) > 0)
		{
			return distance;
		}
		
		distance += currentPiece.length - myCar.piecePosition.inPieceDistance;
				
		int runningPieceIndex = myCar.piecePosition.pieceIndex+1;
		Piece runningPiece = RaceState.I().getPiece(runningPieceIndex++);
		while(runningPiece.length > 0)
		{
			distance += runningPiece.length;
			runningPiece = RaceState.I().getPiece(runningPieceIndex++);
		}
		
		if(distance <0 )
			return 0;
		return distance;
	}

	double getDistanceToPiece(CarPosition myCar, Piece piece)
	{
		double distance = 0;
		
		Piece currentPiece = RaceState.I().getPiece(myCar.piecePosition.pieceIndex);
		if(Math.abs(currentPiece.angle) > 0)
		{
			return distance;
		}
		
		distance += currentPiece.length - myCar.piecePosition.inPieceDistance;
				
		int runningPieceIndex = myCar.piecePosition.pieceIndex+1;
		Piece runningPiece = RaceState.I().getPiece(runningPieceIndex++);
		while(runningPiece != piece)
		{
			distance += runningPiece.length;
			runningPiece = RaceState.I().getPiece(runningPieceIndex++);
		}
		
		if(distance <0 )
			return 0;
		return distance;
	}

	
	Piece getNextCurve(CarPosition myCar)
	{
		
		int runningPieceIndex = myCar.piecePosition.pieceIndex;
		Piece runningPiece = RaceState.I().getPiece(runningPieceIndex++);
		while(runningPiece.length > 0)
		{
			runningPiece = RaceState.I().getPiece(runningPieceIndex++);
		}
		return runningPiece;
	}
	
	double getTicksToCurve(double currentSpeed, double distance) {

		int ticks = (int)(distance / currentSpeed);
		return ticks;
	}
	
	Piece getNextSwitch(CarPosition myCar)
	{
		
		int runningPieceIndex = myCar.piecePosition.pieceIndex;
		Piece runningPiece = RaceState.I().getPiece(runningPieceIndex++);
		while(!runningPiece.switchi)
		{
			runningPiece = RaceState.I().getPiece(runningPieceIndex++);
		}
		return runningPiece;
	}	
	Piece getSlowestCurve(CarPosition myCar)
	{
		int runningPieceIndex = myCar.piecePosition.pieceIndex;
		Piece runningPiece = RaceState.I().getPiece(runningPieceIndex++);
		Piece slowest = runningPiece;
		int lane = myCar.piecePosition.lane.endLaneIndex;
		while(runningPiece.length > 0)
		{
			runningPiece = RaceState.I().getPiece(runningPieceIndex++);
		}
		while(runningPiece.length == 0)
		{
			if(slowest.getMaxSpeed(lane,lane) > runningPiece.getMaxSpeed(lane,lane))
			{
				slowest = runningPiece;
			}
			runningPiece = RaceState.I().getPiece(runningPieceIndex++);
		}
		return slowest;
	}
	
	
	//=(vakio-G5*vakio*(L4/9))-(1-G5)*(vakio + vakio/10*(L4))
	double getBrakePower(double speed)
	{
		RaceState state = RaceState.getInstance();
		
		// magic ;)
		return -state.acceleration*0.1*speed;
	}
}
