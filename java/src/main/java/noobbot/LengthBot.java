package noobbot;

import java.util.ArrayList;
import java.util.List;

public class LengthBot extends Noobbot {

	boolean made_selection = false;
	boolean useShort;	
	
	private RaceState raceState = RaceState.getInstance();
	private Throttle throttle;
	
	public LengthBot(boolean useShort) {
		this.useShort = useShort;
		this.throttle = new Throttle(0.0);
	}
	
	@Override
	SendMsg nextMove( )
	{		
		CarPosition myCar = raceState.getMyCar();
		
		int pieceIndex = myCar.piecePosition.pieceIndex;
		int nextIndex = pieceIndex + 1 ;
		
		Piece current =  RaceState.I().getPiece(pieceIndex);
		int startLane = myCar.piecePosition.lane.endLaneIndex;
				
		
		Piece next = RaceState.I().getPiece(nextIndex);
		
		Piece nextSwitch = getNextSwitch(myCar);
		double distanceToSwitch = getDistanceToPiece(myCar, nextSwitch);
		double ticksToSwitch = getTicksToCurve(myCar.advance, distanceToSwitch);
		
		if(!made_selection && next.switchi && ticksToSwitch < 3)
		{
			made_selection = true;
			int newLane = getNewLane(nextIndex,startLane, 0,0);
			if(newLane < startLane) {
				return new SwitchLane(SwitchLane.LEFT);
			} else if (newLane > startLane){
				return new SwitchLane(SwitchLane.RIGHT);
			}
		}

		if(current.switchi && made_selection)
		{
			made_selection = false;
		}

		return null;
	}

	double calculateLaneLength(int pieceIndex, int incomingLane, int switchCount, int maxSwitchCount)
	{
		Piece thisPiece = RaceState.I().getPiece(pieceIndex);
		
		if(switchCount == maxSwitchCount)
		{
			return 0;
		}
		
		if(thisPiece.switchi)
		{
			switchCount ++;
		}

		class Length 
		{
			double myLength;
			double childLength;
			public Length(double myLength, double childLength) {
				this.myLength = myLength;
				this.childLength = childLength;
			}
			public double getLength()
			{
				return myLength + childLength;
			}
		}
		
		List<LaneInfo> lengths = thisPiece.getLaneInfos();
		List<Length> selectionArray = new ArrayList<Length>();

		for(int n=0, count = lengths.size(); n<count; ++n)
		{
			LaneInfo length = lengths.get(n);
			if(length.startLane == incomingLane) {
				double increase = calculateLaneLength(pieceIndex+1,length.endLane, switchCount, maxSwitchCount);
				increase += thisPiece.hasSlowerCar(length.endLane);
				
				selectionArray.add(new Length(length.laneLength/length.laneSpeed, increase));
				
		//		System.out.println("index(" + pieceIndex + ") startLane(" + length.startLane+ ") endlane(" + length.endLane + ") lanelength (" + (int)length.laneLength + ") increaseLength(" + (int)increase);
			}
		}		
		
		if(useShort) {
			double min = 9999999999999999.0;
			for(int n=0, count = selectionArray.size();n<count;++n)
			{
				min = Math.min(selectionArray.get(n).getLength(), min);
			}
			return min;
		} else {
			double max = 0.0;
			for(int n=0, count = selectionArray.size();n<count;++n)
			{
				max = Math.max(selectionArray.get(n).getLength(), max);
			}
			return max;
			
		}
	}
	
	int getNewLane(int pieceIndex, int startLane, double current_length, int switchCount)
	{
		Piece thisPiece = RaceState.I().getPiece(pieceIndex);
		if(thisPiece.switchi)
		{
			switchCount++;
			
			List<LaneInfo> lengths = thisPiece.getLaneInfos();

			class Selection
			{
				public double length;
				public int nextLane;
				
				public Selection(double length, int nextLane) {
					this.length = length;
					this.nextLane = nextLane;
				}
			}

			List<Selection> selectionArray = new ArrayList<Selection>();

			for(int n=0, count = lengths.size(); n<count; ++n)
			{
				LaneInfo length = lengths.get(n);
				if(length.startLane == startLane) {
					double tempLength = length.laneLength/length.laneSpeed + calculateLaneLength(pieceIndex+1, length.endLane, switchCount, RaceState.I().race.track.lanes.size());
					tempLength += thisPiece.hasSlowerCar(length.endLane);

					Selection temp = new Selection(tempLength, length.endLane);
					selectionArray.add(temp);
					
					//System.out.println("sl(" + length.startLane+ ") el(" + length.endLane + ") length(" + (int)tempLength + ")");
				}
			}

			int newLane = startLane;
			
			if(useShort) {
				double min = 9999999999.9;
				for(int n=0, count = selectionArray.size(); n<count; ++n)
				{
					Selection sel = selectionArray.get(n);
					if(sel.length < min)
					{
						min = sel.length;
						newLane = sel.nextLane;
					}
				}
				//System.out.println("lane " + startLane + " to " + newLane + " length " + min);
				return newLane;
			} else {
				double max = 0.0;
				for(Selection sel : selectionArray)
				{
					if(sel.length > max)
					{
						max = sel.length;
						newLane = sel.nextLane;
					}
				}
				//System.out.println("lane " + startLane + " to " + newLane + " length " + max);
				return newLane;
				
			}
		}
		
		return startLane;
	}
}
