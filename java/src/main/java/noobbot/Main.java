package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;



public class Main {
	
	int testbotstate =1;
	Race race = null;;
	static RaceState state = RaceState.getInstance();
    
	LengthBot botLength = null;
	SpeedBot botSpeed = null;
	MeasureBot botMeasure = null;
	TurboBot botTurbo = null;

	List<Noobbot> bots = new ArrayList<Noobbot>();
	
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botKey = args[3];
        String botName = new String(args[2]);
        state.setBotName(botName);

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        
        //System.out.println(createRace.toJson());
        
        if(args.length <= 4)
        {
        	new Main(reader, writer, new Join(botName, botKey));
        } else {
            CreateRace createRace = null;
            
            if(args[4].equals("create")) {
            	System.out.println("creating new game");
            	createRace = new CreateRace(args[5],args[6],botName,botKey,Integer.parseInt(args[7]));
            } else if(args[4].equals("join")) {
            	System.out.println("joining new game");
            	createRace = new JoinRace(args[5],args[6],botName,botKey,Integer.parseInt(args[7]));
            } else {
            	System.out.println("usage: Main.java server port name key create/join track passwd cars");
            }
            
            new Main(reader, writer, createRace);
        }
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        botLength = new LengthBot(true);
        bots.add(botLength);
        
        botSpeed = new SpeedBot();
        bots.add(botSpeed);

        botMeasure = new MeasureBot();
        bots.add(botMeasure);

        botTurbo = new TurboBot();
        bots.add(botTurbo);

        int counter = 0;
        
        while((line = reader.readLine()) != null) {
        try {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
       //     System.out.println(line);
            if (msgFromServer.msgType.equals("carPositions")) {
            	
            	if(state.getBotName().equals("testbot"))
            	{
            		if(testbotstate==1) {
            			send(new Throttle(0.5));
            			//testbotstate=0;
            		} else {
            			//send(new SwitchLane(SwitchLane.LEFT));
            			send(new Ping());
            		}
            	}
            	
            	if(counter < 2)
            	{
                    System.out.println(line);
            	}
            	counter++;
            	PositionMsgWrapper position = gson.fromJson(line,PositionMsgWrapper.class);
            	
            	state.updateState(position.data);
            	if(!state.turboAvailable)
            	{
            		state.turboTicks--;
            	}
            	//make the decision, which bot should be used
            	makeDecision();
                

            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
                System.out.println(line);
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                System.out.println(line);
                InitMsgWrapper initmsg = gson.fromJson(line.replace("switch", "switchi"),InitMsgWrapper.class);
                if(race == null)
                {
	                race = initmsg.data.race;
	                
	                // set race
	                state.setRace(race);
                }
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
                System.out.println(line);
            } else if (msgFromServer.msgType.equals("crash")) {
                System.out.println("crashed");
                System.out.println(line);
                final CrashMsgWrapper crashMsgFromServer = gson.fromJson(line, CrashMsgWrapper.class);
                
                CrashData crashData = crashMsgFromServer.data;
                CarPosition myCar = state.getMyCar();
                if(crashData.name.equals(myCar.id.name))
                {
                	//we crashed, limix max speed fo the previous tracks by 10%
                	Piece previousPiece = state.getPiece(myCar.swithedPieceIndex);
    				LaneInfo updateInfo = previousPiece.getLaneInfo(myCar.switchedStart, myCar.switchedEnd);
    				updateInfo.laneSpeed *= 0.9;	
    				
                	Piece currentPiece = state.getPiece(myCar.swithedPieceIndex);
    				LaneInfo currentUpdateInfo = currentPiece.getLaneInfo(myCar.piecePosition.lane.startLaneIndex, myCar.piecePosition.lane.startLaneIndex);
    				currentUpdateInfo.laneSpeed *= 0.9;	
    				                	
                }
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
                System.out.println("turboooooo!");
                System.out.println(line);
                final TurboMsgWrapper turboMsgFromServer = gson.fromJson(line, TurboMsgWrapper.class);

                state.turboAvailable = true;
                state.turboTicks = turboMsgFromServer.data.turboDurationTicks;
                state.turboPower = turboMsgFromServer.data.turboFactor;
            
            
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println(line);
                System.out.println("Race start");
                send(new Ping());
            } else {
                System.out.println(msgFromServer.msgType + " : " + line);
                //send(new Ping());
            }
        } catch (Exception e)
        {
        	send(new Ping());
        }
        
        }
    }

    
    void makeDecision()
    {
    	
    	SendMsg decision = null;
    	
    	//run measuring
    	decision = botMeasure.nextMove(); 

    	
    	//just in time route to shortest... er... fastest
    	if(null == decision)
    	{
    		decision = botLength.nextMove(); 
    	}
    	
    	//useTurbo?
    	if(null == decision)
    	{
    		decision = botTurbo.nextMove(); 
    	}
    	
    	//if no other messages yet, check speed
    	if(null == decision)
    	{
    		decision = botSpeed.nextMove();
    	}
    	
    	
    	if(decision.getClass() == Throttle.class)
    	{
    		Throttle temp = (Throttle)decision;
	    	// Update our next known throttle
	    	state.myCar.setNextThrottle(temp.getThrottle());
    	}
    	
    	send(decision);
    }
    
    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

class InitMsgWrapper {
    public String msgType;
    public InitData data;
}

class InitData {

	public Race race;
	
}


class PositionMsgWrapper {
	public String msgType;
	public List<CarPosition> data;
}


class CrashMsgWrapper {
	public String msgType;
	public CrashData data;
}

class CrashData {
	public String name;
	public String color;
}

class TurboMsgWrapper {
	public String msgType;
	public TurboData data;
}

class TurboData {
	public int turboDurationTicks;
	public double turboFactor;
}

class CreateRace extends SendMsg {
	public String trackName;
	public String password;
	public Join botId;
	public int carCount;
	
	@Override
    protected String msgType() {
        return "createRace";
    }

	public CreateRace(String trackName, String password, String name, String key,
			int carCount) {
		this(trackName, password, new Join(name,key), carCount);
	}
	
	public CreateRace(String trackName, String password, Join botId,
			int carCount) {
		this.trackName = trackName;
		this.password = password;
		this.botId = botId;
		this.carCount = carCount;
	}	
    

}

class JoinRace extends CreateRace {	
	@Override
    protected String msgType() {
        return "joinRace";
    }

	public JoinRace(String trackName, String password, String name, String key,
			int carCount) {
		super(trackName, password, new Join(name,key), carCount);
	}    
}
//{"msgType": "createRace", "data": {
//	  "botId": {
//	    "name": "schumacher",
//	    "key": "UEWJBVNHDS"
//	  },
//	  "trackName": "hockenheimring",
//	  "password": "schumi4ever",
//	  "carCount": 3
//	}}

