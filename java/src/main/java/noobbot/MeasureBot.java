package noobbot;

public class MeasureBot extends Noobbot
{
	
	boolean ready = false;

	@Override
	SendMsg nextMove() 
	{
		
		if(ready)
		{
			return null;
		}
		
		RaceState state = RaceState.getInstance();
		if(state.acceleration == 0)
		{
			state.acceleration = state.getMyCar().advance;
			if(state.acceleration > 0)
				ready = true;
		}
		
		return new Throttle(1.0);
		
	}

	public void crashHappened(CrashData data) 
	{
		RaceState state = RaceState.getInstance();
	
		CarPosition crashedCar = state.getCar(data.name, data.color);
		
		Piece crashLocation = state.getPiece(crashedCar.piecePosition.pieceIndex);
		LaneInfo info = crashLocation.getLaneInfo(crashedCar.piecePosition.lane.startLaneIndex, crashedCar.piecePosition.lane.endLaneIndex);
		
		System.out.println("derp,speed," + crashedCar.advance + ",angle," + crashedCar.angle + ",radius," + info.radius + ",r2," + crashLocation.radius);
	}

}
