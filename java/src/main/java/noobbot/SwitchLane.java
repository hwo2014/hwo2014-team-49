package noobbot;

class SwitchLane extends SendMsg {
    private int value;

    public static final int LEFT = 1;
    public static final int RIGHT = 2;
    
    public SwitchLane(int value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        if(value == LEFT) 
        { 
        	System.out.println("choosing Left");
        	return "Left";
        } else {
        	System.out.println("choosing Right");
        	return "Right";
        }
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}