package noobbot;

public class TurboBot extends Noobbot {

	@Override
	SendMsg nextMove() {

		RaceState state = RaceState.getInstance();
		if(state.turboAvailable && state.getMyCar().lastThrottle == 1.0 && getDistanceToCurve(state.getMyCar()) > 250)
		{
			state.turboAvailable = false;
			return new UseTurbo("jeiiii");
		}
		
		return null;
	}
}
