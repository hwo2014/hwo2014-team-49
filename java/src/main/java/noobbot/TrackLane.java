package noobbot;

class TrackLane {
	public final int distanceFromCenter;
	public final int index;
	
	TrackLane(int distanceFromCenter, int index) {
		this.distanceFromCenter = distanceFromCenter;
		this.index = index;
	}
	
	public TrackLane(final TrackLane lane) {
		this(lane.distanceFromCenter,lane.index);
	}
}