package noobbot;

import java.util.ArrayList;
import java.util.List;

class Piece {
	public final double length;
	public final boolean switchi;
	public final double radius;
	public final double angle;
	
	private List<LaneInfo> laneInfos = null;
	private List<LaneInfo> straightLanes = null;
	
	Piece(double length, boolean switchi, double radius, double angle) {
		this.length = length;
		this.switchi = switchi;
		this.radius = radius;
		this.angle = angle;
	}
	
	public Piece(final Piece piece) {
		this (piece.length,piece.switchi,piece.radius,piece.angle);
	}

	public void preCalculate(Track track){
		// Create LaneInfos
		laneInfos = new ArrayList<LaneInfo>();
		straightLanes = new ArrayList<LaneInfo>();
		
		// straight line
		if(isStraight()) {
			int n=0, count=track.lanes.size();
			for(;n<count;n++)
			{
				laneInfos.add(new LaneInfo(n,n,length,0));
				straightLanes.add(new LaneInfo(n,n,length,0));
				
				if(switchi && (n+1) < count)
				{
					
//					10, 5 -> 5
//					-10, 5 -> -15
//					5,-5 -> 10
//					-10, -5 -> -5 
					double distance_between = track.getLane(n).distanceFromCenter - track.getLane(n+1).distanceFromCenter;

					double new_length = Math.sqrt(length*length + distance_between*distance_between);							  

					
					laneInfos.add(new LaneInfo(n,n+1,new_length,0));
					laneInfos.add(new LaneInfo(n+1,n,new_length,0));
				}
			}
		} else {
			int n=0, count=track.lanes.size();
			for(;n<count;n++)
			{
				double direction = -1.0;
				if(angle<0)
				{
					direction*=-1.0;
				}
				
				double newLength = Math.abs(Math.toRadians(angle)*(radius + direction*track.getLane(n).distanceFromCenter));
								
				laneInfos.add(new LaneInfo(n,n, newLength, radius + direction*track.getLane(n).distanceFromCenter));
				straightLanes.add(new LaneInfo(n,n, newLength, radius + direction*track.getLane(n).distanceFromCenter));
				if(switchi && (n+1) < count)
				{
					double calculated_radius = radius +  direction*
												(track.getLane(n).distanceFromCenter +
												 track.getLane(n+1).distanceFromCenter)/2.0;

					newLength = Math.abs(Math.toRadians(angle)*calculated_radius);
					
					laneInfos.add(new LaneInfo(n,n+1,newLength, calculated_radius));
					laneInfos.add(new LaneInfo(n+1,n,newLength, calculated_radius));
				}
			}
		}
	}
	

	List<LaneInfo> getLaneInfos()
	{
		if(laneInfos == null) {
			//System.out.println("#ERR: getLaneInfos() called without calling preCalculate()!");
		}
		
		return laneInfos;
	}
	
	LaneInfo getLaneInfo(int laneId)
	{
		return straightLanes.get(laneId);
	}
	
	LaneInfo getLaneInfo(int startLane, int endLane)
	{
		for( LaneInfo info : laneInfos)
		{
			if(info.startLane == startLane && info.endLane == endLane)
			{
				return info;
			}
		}
		return null;
	}

	
	private boolean isStraight() {
		return length>0;
	}

	public double getMaxSpeed(int laneStart, int laneEnd) 
	{

		LaneInfo info = getLaneInfo(laneStart, laneEnd);
		if(length>0) {
			return 100;
		} else {
			return info.laneSpeed;
		}
	}

	public double hasSlowerCar(int lane) 
	{
		RaceState state = RaceState.getInstance();
		for(CarPosition position : state.carPositions)
		{
			if(position.piecePosition.lap < state.getMyCar().piecePosition.lap && position.piecePosition.lane.endLaneIndex == lane && this == state.getPiece(position.piecePosition.pieceIndex) )
			{
				return 50;
			}
		}
		
		return 0;
	}
	
}

class LaneInfo {
	public int startLane;
	public int endLane;
	public double laneSpeed;
	public double laneLength;
	public double radius;
	
	
	public LaneInfo(int startLane, int endLane, double laneLength, double radius) {
		this.startLane = startLane;
		this.endLane = endLane;
		this.laneSpeed = 1;//learn from track
		this.laneLength = laneLength;
		this.radius = radius;
	}
}