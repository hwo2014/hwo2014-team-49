package noobbot;

class Race {
    public final Track track;
    public final Object cars;
    public final Object raceSession;

    Race(final Track track, final Object cars, final Object raceSession) {
        this.track = track;
        this.cars = cars;
        this.raceSession = raceSession;
    }

    public Race(final Race race) {
        this(race.track,race.cars,race.raceSession);    
    }
}