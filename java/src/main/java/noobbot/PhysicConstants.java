package noobbot;

public interface PhysicConstants {

	/**
	 * Get the maximal angle the car can have before slipping
	 * @return angle in radians and always positive
	 */
	float maxCarAngle();
	
	/**
	 * Get the value of maximal deceleration the car can have
	 * @return float value that represents maximal deceleration
	 */
	float maxDeceleration();
	
}
