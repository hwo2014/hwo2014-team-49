package noobbot;

public class PositionBot extends Noobbot {

	@Override
	SendMsg nextMove( )
	{
		CarPosition my = RaceState.I().getMyCar();

		Race race = RaceState.getInstance().race;
		
		int pieceIndex = my.piecePosition.pieceIndex;
		int nextIndex = (my.piecePosition.pieceIndex + 1) %(race.track.pieces.size());
		
		Piece current = race.track.pieces.get(pieceIndex);
		Piece next = race.track.pieces.get(nextIndex);
		
		if(current.length > 0 && next.length > 0)
			return new Throttle(0.8);
		else if(current.length > 0 && next.length == 0)
			return new Throttle(0.1);
		else
			return new Throttle(0.7);
		
	}	
}
