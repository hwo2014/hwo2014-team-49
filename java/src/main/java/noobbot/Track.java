package noobbot;

import java.util.List;

class Track {
	public String id;
	public String name;
	
	public List<Piece> pieces;
	public List<TrackLane> lanes;

	public Object startingPoint;
	
	Track(String id, String name, List<Piece> pieces, List<TrackLane> lanes,
			Object startingPoint) {
		this.id = id;
		this.name = name;
		this.pieces = pieces;
		this.lanes = lanes;
		this.startingPoint = startingPoint;
	}
	
	

	public Track(final Track track) {
		this(track.id,track.name,track.pieces,track.lanes,track.startingPoint);
	}	
	
	TrackLane getLane(int index)
	{
		for(TrackLane lane : lanes)
		{
			if(lane.index == index)
			{
				return lane;
			}
		}
		return null;
	}
	
}