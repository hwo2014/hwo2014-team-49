package noobbot;

class UseTurbo extends SendMsg {
    String message;
	
	
    public UseTurbo(String message) {
        this.message = message;
    }

    @Override
    protected Object msgData() {
    	return message;
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}