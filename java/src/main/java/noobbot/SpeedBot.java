package noobbot;

public class SpeedBot extends Noobbot {

	
	RaceState state = RaceState.getInstance();
	
	@Override
	SendMsg nextMove() 
	{
		CarPosition myCar = state.getMyCar();
		double distanceToCurve = getDistanceToCurve(myCar);
		Piece nextCurve = getNextCurve(myCar);
		Piece slowestCurve = getSlowestCurve(myCar);
		Piece currentPiece = state.getPiece(myCar.piecePosition.pieceIndex);
		double maxSpeed = nextCurve.getMaxSpeed(myCar.piecePosition.lane.endLaneIndex,myCar.piecePosition.lane.endLaneIndex);
		double maxSlowestCurveSpeed = slowestCurve.getMaxSpeed(myCar.piecePosition.lane.endLaneIndex,myCar.piecePosition.lane.endLaneIndex);
		double distanceSlowestCurve = getDistanceToPiece(myCar, slowestCurve);
		double currentSpeed = myCar.advance;
		double currentThrottle = myCar.lastThrottle;
		double currectAcceleration = myCar.acceleration;
		double ticksToCurve = getTicksToCurve(currentSpeed, distanceToCurve);
		double ticksToSlowestCurve = getTicksToCurve(currentSpeed, distanceSlowestCurve);
		double ticksToBreak = getTicksToBreak(currentSpeed, maxSpeed); 
		double ticksToSlowestBreak = getTicksToBreak(currentSpeed, maxSlowestCurveSpeed); 
		//System.out.println("dist(" + distanceToCurve + ") ms(" + maxSpeed + ") cs(" + currentSpeed + ") ct(" + currentThrottle + ") acc(" + currectAcceleration + ") ttc(" + ticksToCurve + ")");
		//System.out.println("gerp,dist," + distanceToCurve + ",ms," + maxSpeed + ",cs," + currentSpeed + ",ct," + currentThrottle + ",acc," + currectAcceleration + ",ttc," + ticksToCurve + ",ttb," + ticksToBreak);
		
		
		
		if(myCar.advance < 1)
		{
			return new Throttle(1.0);
		}

		//break at full, if needed
		if(currentPiece.length > 0)
		{
			if((ticksToBreak > ticksToCurve && myCar.advance > maxSpeed) || 
			   (ticksToBreak > ticksToSlowestCurve && myCar.advance > maxSlowestCurveSpeed))
			{
				//System.out.println("breaking: ttb(" + ticksToBreak + ") ttc(" +ticksToCurve + ") speed(" + myCar.advance + ") mspeed(" + maxSpeed + ")");
				return new Throttle(0);
			}
		
		// straight  at full speed if not using brakes
			return new Throttle(1.0);
		}
		
		if((ticksToBreak > ticksToCurve && myCar.advance > maxSpeed) || 
		   (ticksToBreak > ticksToSlowestCurve && myCar.advance > maxSlowestCurveSpeed))
		{
			//System.out.println("breaking: ttb(" + ticksToBreak + ") ttc(" +ticksToCurve + ") speed(" + myCar.advance + ") mspeed(" + maxSpeed + ")");
			return new Throttle(0);
		}

		double laneSpeed = state.getPiece(myCar.piecePosition.pieceIndex).getLaneInfo(myCar.piecePosition.lane.startLaneIndex, myCar.piecePosition.lane.endLaneIndex).laneSpeed;
		
			
		//more speed if not drifting
		if(myCar.abs_angle < 5)
		{
			return new Throttle(1.0);
		}
		//corner drive skillz
		
		if(myCar.delta_abs_angle > 5 || myCar.delta_abs_angle > 40)
		{
			return new Throttle(0);
		}

		if(myCar.advance < laneSpeed*0.50)
		{
			return new Throttle(1.0);
		}

		if(myCar.abs_angle < 30)
		{
			return new Throttle(Math.min(0.83,myCar.lastThrottle+0.083));
		}


		if(myCar.delta_abs_angle < -5)
		{
			return new Throttle(1.0);
		}

		if(myCar.abs_angle > 30)
		{
			return new Throttle(myCar.lastThrottle);
		}
		
		return new Throttle(0.5 - (myCar.delta_abs_angle/10) );
	}
	
	private double getSpeedAfterTics(int ticksToCurve, double currentSpeed,
			double currectAcceleration)
	{	
		if(ticksToCurve>0)
			return getSpeedAfterTics(ticksToCurve-1, currentSpeed-currectAcceleration, currectAcceleration);
		else 
			return 0;
	}



	double getMaxSpeedForNextTick(CarPosition myCar)
	{
		double distance = getDistanceToCurve(myCar);
		
		
		return distance;
	}


	int getTicksToBreak(double currentSpeed, double targetSpeed)
	{
		//System.out.println(currentSpeed + ":" + targetSpeed);
		
		double tempAcc = getBrakePower(currentSpeed);
		
		if(tempAcc == 0)
		{
			return 1;
		}
		if(currentSpeed < targetSpeed )  return 1;
		if(tempAcc > 0)
		{
			tempAcc *= -1;
		}
		return 1 + getTicksToBreak(currentSpeed + tempAcc,  targetSpeed);
	}
	
}
