package noobbot;

class PiecePosition {
	public int pieceIndex;
	public double inPieceDistance;
	public PieceLane lane;
	public int lap;
	
	/**
	 * Empty constructor to prevent using null in other parts of the code
	 */
	public PiecePosition() {
		pieceIndex = 0;
		inPieceDistance = 0.0;
		lap = 0;
	}
	
}