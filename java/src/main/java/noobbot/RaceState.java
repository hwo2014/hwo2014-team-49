package noobbot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RaceState {
	private static RaceState instance = null;

	public Race race;
	String botName;
	double acceleration = 0.0;

	//info about the cars runnign on the track
	List<CarPosition> carPositions = new ArrayList<CarPosition>();
	//quick access to my car
	CarPosition myCar = null;

	//cars in the leading order
	List<CarPosition> leaderBoard = new ArrayList<CarPosition>();

	//if false also count down turbo counter
	public boolean turboAvailable = false;
	public double  turboPower = 1;
	public int turboTicks = 0;

	//singleton constructor
	protected RaceState() 	{}

	//Singleton instance retrieval
	public static RaceState getInstance() {
		if(instance == null) {
			instance = new RaceState();
		}
		return instance;
	}
	//Singleton instance retrieval
	public static RaceState I() {
		if(instance == null) {
			instance = new RaceState();
		}
		return instance;
	}

	//updates the changes in cars on the track, called for every gametick
	void updateState(List<CarPosition> cars)
	{
		if(carPositions.size() == 0)
		{
			carPositions.addAll(cars);
			leaderBoard.addAll(carPositions);
			for(int n=0;n<carPositions.size();n++)
			{
				myCar = carPositions.get(n); 
				if(myCar.id.name.equals(getBotName()))
				{
					break;
				}
			}
		} else {
			//update values
			for(CarPosition rPosition: carPositions)
			{
				for(CarPosition updated : cars ) {
					if(rPosition.id.name.equals(updated.id.name))
					{
						rPosition.updateFrom(updated, race.track);
					}
				}
			}
		}

		//rearrange leaderboard with bubble sort
		boolean switched = false;
		do {
			switched = false;

			int firstIndex = 0;
			CarPosition first = leaderBoard.get(firstIndex);
			int firstPosition = first.piecePosition.lap*race.track.pieces.size() + first.piecePosition.pieceIndex;

			for(int n=1, count = leaderBoard.size(); n<count;++n )
			{
				CarPosition next = leaderBoard.get(n);
				int nextPosition = next.piecePosition.lap*race.track.pieces.size() + next.piecePosition.pieceIndex;

				if( nextPosition > firstPosition ||
						(nextPosition == firstPosition &&
						next.piecePosition.inPieceDistance > first.piecePosition.inPieceDistance) 
						)
				{
					Collections.swap( leaderBoard, firstIndex, n);
					switched = true;
				}

				firstIndex = n;
				firstPosition = nextPosition;
				first = next;
			}

		} while(switched);


		//
		for(CarPosition car : leaderBoard) {
			if(car.swithedPiece)
			{
				Piece update = getPiece(car.pre_swithedPieceIndex);
				LaneInfo updateInfo = update.getLaneInfo(car.pre_switchedStart, car.pre_switchedEnd);
				if( updateInfo.laneSpeed < car.pre_previousAdvance)
				{
					System.out.println("new speed index(" + car.pre_swithedPieceIndex + "(" + car.pre_switchedStart + "-" + car.pre_switchedEnd +") :" + car.pre_previousAdvance);
					updateInfo.laneSpeed = car.pre_previousAdvance;
				}
				for(LaneInfo lane: update.getLaneInfos())
				{
					if(lane.radius >= updateInfo.radius && lane.laneSpeed < updateInfo.laneSpeed)
					{
						lane.laneSpeed = updateInfo.laneSpeed;
					}
				}
			}
		}
	}

	public void setRace(Race race) {
		this.race = race;
		
		// Update all calculate all precalculable stuff on pieces
		for( Piece p : this.race.track.pieces ) {
			p.preCalculate(this.race.track);
		}
	}

	public Piece getPiece(int index)
	{
		int fixed_index = index %(race.track.pieces.size());

		return race.track.pieces.get(fixed_index);
	}


	public String getBotName() {
		return botName;
	}

	public void setBotName(String botName) {
		this.botName = botName;
	}


	public CarPosition getMyCar() 
	{
		return myCar;
	}

	public CarPosition getCar(String name, String color) 
	{
		for ( CarPosition car : carPositions)
		{
			if(car.id.name.equals(name) && car.id.color.equals(color))
			{
				return car;
			}
		}
		return null;
	}

}
