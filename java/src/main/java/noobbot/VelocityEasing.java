package noobbot;

public class VelocityEasing {

	/**
	 * All supported easing function types
	 * @author stenhamm
	 *
	 */
	public enum EasingType {
		LINEAR
	}
	
	private float Vmax;
	private EasingType type;
	private PhysicConstants pc;
	
	private float a, b;
	
	/**
	 * Creates a new velocity easing function which is linear
	 * @param Vmax The velocity that can be had when angle of the car is already at max
	 */
	public VelocityEasing(float Vmax, PhysicConstants pc) {
		this(Vmax, pc, EasingType.LINEAR);
	}
	
	/**
	 * Create new velocity easing function of given type.
	 * @param Vmax The velocity that can be had when angle of the car is already at max
	 * @param type Type of the easing function to be used
	 */
	public VelocityEasing(float Vmax, PhysicConstants pc, EasingType type) {
		this.Vmax = Vmax;
		this.type = type;
		
		initVariables();
	}
	
	public float ease(float angle) {
		// Just assume linear function now
		// TODO: Implement and test different functions. Also, turn can come too fast for 
		// angle based easing to work. This requires other easing function that takes
		// into consideration upcoming turn and our velocity as well as max deceleration.
		
		return linearEase(angle);
	}
	
	// Initializes variables for given function type
	private void initVariables() {
		switch(type)
		{
		case LINEAR:
		{
			a = 1.0f - Vmax;
			b = 1.0f;
			break;
		}
		default:
			System.out.println("#ERR: Variable initialization for VelocityEasing went to default!");
		}
	}
	
	// Linear function is of type 'y = ax + b', where 
	//		x is normalized angle x := [0,1],
	// 		a is the velocity drop at max angle a := 1.0 - Vmax
	// 		b is base velocity b := 1.0
	private float linearEase(float angle) {
		float na = Math.abs(angle) / pc.maxCarAngle();
		if(na > 1.0f)
		{
			System.out.println("#ERR: Car angle exceeds expected maximum " + angle + " > " + pc.maxCarAngle() );
			na = 1.0f;
		}
		
		return a*na+b;
	}
	
}
