package noobbot;

class CarPosition {
	public CarId id;
	public double angle;
	public PiecePosition piecePosition = new PiecePosition();
	
	// Calculated variables
	public double velocity = 0.0;
	public double acceleration = 0.0;
	
	public double lastThrottle = 0.0;
	public double previousThrottle = 0.0;
	
	public double advance = 0.0; 
	private double previousAdvance = 0.0;
	public double pre_previousAdvance = 0.0;
	public double delta_abs_angle = 0;
	public double abs_angle;
	public boolean swithedPiece = false;
			
	private int previousPieceIndex=0;
	public int swithedPieceIndex=0;
	public int switchedStart=0;
	public int switchedEnd=0;

	public int pre_swithedPieceIndex=0;
	public int pre_switchedStart=0;
	public int pre_switchedEnd=0;

	public void updateFrom(CarPosition updated, Track track) {
		previousAdvance = advance;
		previousThrottle = lastThrottle;
		
		if(previousPieceIndex != updated.piecePosition.pieceIndex)
		{
			swithedPiece = true;
			//update upto two indexes back
			pre_previousAdvance = previousAdvance;

			pre_swithedPieceIndex = swithedPieceIndex;
			swithedPieceIndex = previousPieceIndex;
			
			pre_switchedStart = switchedStart;
			switchedStart = piecePosition.lane.startLaneIndex;
			
			pre_switchedEnd = switchedEnd;
			switchedEnd = piecePosition.lane.endLaneIndex;
		} else {
			swithedPiece = false;
		}
		
		previousPieceIndex = updated.piecePosition.pieceIndex;
		advance = 0.0;
		// In case we didn't yet change piece
		if (piecePosition.pieceIndex == updated.piecePosition.pieceIndex) {
			advance = updated.piecePosition.inPieceDistance - piecePosition.inPieceDistance;
		}
		else {
			// We did change the piece so find out how much there was left on last piece
			int pId = piecePosition.pieceIndex;
			int lId = piecePosition.lane.endLaneIndex;
			
			advance += track.pieces.get(pId).getLaneInfo(lId).laneLength - piecePosition.inPieceDistance;
			advance += updated.piecePosition.inPieceDistance;
		}
		
		if(advance == 0.0)
		{
			//crash has occurred, so was running too fast, lest not update to crashing values, like we did in qualification
			pre_previousAdvance = 0.0;
			previousAdvance = 0.0;
		}
		
		//System.out.println("adv(" + advance +") ang(" + angle + ")");
		
		// Velocity v = dx / dt, where dx is advance and dt is tick -> distance units / tick
		double newVelocity = advance;
		
		angle = updated.angle;
		delta_abs_angle = Math.abs(angle) - abs_angle; 
		abs_angle = Math.abs(angle);
		piecePosition = updated.piecePosition;
		
		// Acceleration a = dv / dt, where dv is velocity change and dt is tick -> distance units / tick^2
		acceleration = newVelocity - velocity; 
		velocity = newVelocity;
		
		
		//
	}
		
	public void setNextThrottle(double throttle)
	{
		//System.out.println( "Throttle = (" + lastThrottle + "), Velocity = (" + velocity + "), acceleration = (" + acceleration + ")" );
		lastThrottle = throttle;
	}
	
}