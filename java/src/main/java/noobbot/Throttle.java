package noobbot;

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }
    
    public double getThrottle()
    {
    	return value;
    }
    
    @Override
    protected Object msgData() {
    	//System.out.println("t(" + (value) + ")");
    	if(value != 1.0 && !RaceState.I().turboAvailable && RaceState.I().turboTicks > 0) 
    	{
    		return value/RaceState.I().turboPower;
    	}
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}